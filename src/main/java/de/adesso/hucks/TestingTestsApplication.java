package de.adesso.hucks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingTestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestingTestsApplication.class, args);
	}
}
