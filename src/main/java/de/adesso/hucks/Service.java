package de.adesso.hucks;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@org.springframework.stereotype.Service
public class Service {
	private String apiAdress = "http://localhost:12345/test";
	
	public String requestString() {
        ResponseEntity<String> giro = new RestTemplate()
                .getForEntity(apiAdress, String.class);
		return giro.getBody();
	}
}
