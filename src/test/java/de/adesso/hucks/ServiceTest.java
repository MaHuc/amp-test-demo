package de.adesso.hucks;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.tomakehurst.wiremock.junit.WireMockRule;


@RunWith(SpringRunner.class)
public class ServiceTest {
	
	@Rule
	public WireMockRule wireMockRule = new WireMockRule(12345);
	
	@Test
	public void getStringTest() {
		
		stubFor(get(urlEqualTo("/test"))
				//.withHeader("Accept", equalTo("application/json")) //evtl
				.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type","application/json")
						.withBody("Das ist ein testiger Test")));
		
		String result = new Service().requestString();
		
		assertEquals(result,"Das ist ein testiger Test");
		
		verify(getRequestedFor(urlEqualTo("/test")));
	}
	
}
